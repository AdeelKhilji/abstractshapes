/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shapes;

/**
 *
 * @author Adeel Khilji
 */
public class ShapesSimulation 
{
    public static void main(String[] args)
    {
        //Local variables
        double radius = 8;
        double length = 6, width = 6;
        //Creating a new circle object
        Shape circle = new Circle();
        //Creating a new rectangle object
        Shape rectangle = new Rectangle(length, width);
        
        //Displaying results
        System.out.println("CIRCLE:");
        System.out.printf("RADIUS IS 8\n" + "AREA OF A CIRCLE: %.2f \tCIRCUMFERENCE OF A CIRCLE: %.2f\n",circle.getArea(radius),circle.getPerimeter(radius));
        System.out.println();
        System.out.println("RECTANGLE:");
        System.out.printf("LENGTH IS 6 AND WIDTH IS 6\n" + "AREA OF A RECTANGLE: %.2f \tPERIMETER OF A RECTANGLE: %.2f\n",rectangle.getArea(length),rectangle.getPerimeter(length));
        System.out.println();
    }
}
