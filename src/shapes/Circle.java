/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shapes;

/**
 * Class Circle extends Shape
 * @author Adeel Khilji
 */
public class Circle extends Shape
{
    /**
     * getArea - overridden getter method
     * @param radius double
     * @return double
     */
    @Override
    public double getArea(double radius) 
    {
        return Math.PI * Math.pow(radius, 2);
    }
    /**
     * getPerimeter - overridden getter method
     * @param radius double
     * @return double
     */
    @Override
    public double getPerimeter(double radius) 
    {
        return 2 * Math.PI * radius;
    } 
}
