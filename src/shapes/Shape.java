/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shapes;

/**
 * Abstract Class Shape
 * @author Adeel Khilji
 */
public abstract class Shape 
{
    /**
     * getArea - signature of getter method - abstract
     * @param d double
     * @return double
     */
    public abstract double getArea(double d);
    /**
     * getPerimeter - signature of getter method - abstract
     * @param d double
     * @return double
     */
    public abstract double getPerimeter(double d);
}
