/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shapes;

/**
 *
 * @author Adeel Khilji
 */
public class Rectangle extends Shape
{
    private double length, width;//Declaring instance variables
    
    /**
     * Constructor with two parameters
     * @param length double
     * @param width double
     */
    protected Rectangle(double length, double width)
    {
        this.length = length;
        this.width = width;
    }
    /**
     * getArea - overridden getter method
     * @param length double
     * @return double
     */
    @Override
    public double getArea(double length) 
    {
        return this.width * length;
    }
    
    /**
     * getPerimeter - overridden getter method
     * @param length double
     * @return double
     */
    @Override
    public double getPerimeter(double length) 
    {
        return Math.pow((length + this.width), 2);
    } 
}
